import { MongoClient } from 'mongodb';

const { CACHE_URL, DB_NAME } = process.env;

export async function setupDB() {
  try {
    const conn = await MongoClient.connect(CACHE_URL);

    const isTest = process.env.NODE_ENV === 'test';

    const db = conn.db(`${DB_NAME}${isTest ? '_test' : ''}`);

    return db;
  } catch (error) {
    console.log(error);
  }
}
